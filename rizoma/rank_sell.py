##
## sell_rank.py
## Login : <zeus@lunareja.lunareja>
## Started on  Mon Aug 25 14:53:33 2008 Jonathan Gonzalez V.
## $Id$
##
## Copyright (C) 2008 Rizoma Tecnologia Ltda. <info@rizoma.cl>
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02110-1301 USA
##

import gtk
import rizoma_db
from rizoma.rizoma_db import *

class RankSell:
    def __init__ (self, list_store, date_begin, date_end):
        list_store.clear()

        data = RizomaData ()
        rows = data.Execute ("select * from ranking_ventas (to_date ('%s', 'YYYY-MM-DD'), to_date ('%s', 'YYYY-MM-DD'))" % (date_begin, date_end))

        self.vendidos = 0
        self.costo = 0
        self.contrib = 0

        for row in rows:

            self.vendidos = self.vendidos + row['sold_amount']
            self.costo = self.costo + row['costo']
            self.contrib = self.contrib + row['contrib']

            list_store.append ( [
                    row['descripcion'],
                    row['marca'],
                    row['contenido'],
                    row['unidad'],
                    float (row['amount']),
                    row['sold_amount'],
                    int (row['costo']),
                    int (row['contrib']),
                    float (row['contrib'] / row['costo'] * 100),
                    ] )

    def get_totals (self):
        return (self.vendidos, self.costo, self.contrib)
