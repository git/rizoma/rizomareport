##
## database.py
## Login : <zeus@lunareja.lunareja>
## Started on  Mon Aug 25 14:13:57 2008 Jonathan Gonzalez V.
## $Id$
##
## Copyright (C) 2008 Rizoma Tecnologia Ltda. <info@rizoma.cl>
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02110-1301 USA
##

import psycopg2
import psycopg2.extras

class RizomaData:
    def __init__(self):
        self.conn = psycopg2.connect ('dbname=rizoma user=zeus password=test host=localhost sslmode=require')
        self.cur = self.conn.cursor (cursor_factory=psycopg2.extras.DictCursor)

    def Execute(self, query):
        self.cur.execute (query)

        rows = self.cur.fetchall()

        return rows
