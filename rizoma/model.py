##
## model.py
## Login : <freyes@yoda>
## Started on  Wed Jun 25 12:49:20 2008 Felipe Reyes
## $Id$
##
## Copyright (C) 2008 Felipe Reyes
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
##

import gtk
import gobject

#### orm mapping
## sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Table, Column, Integer, String, DateTime, Boolean, Float, MetaData, ForeignKey
from sqlalchemy.orm import sessionmaker

engine = create_engine('postgres://rizoma@localhost/rizoma_new?port=5434', echo=False)
Session = sessionmaker(bind=engine)
session = Session()

## kiwi
from kiwi.python import enum


Base = declarative_base()
class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    rut = Column(Integer)
    dv = Column(String(1))
    usuario = Column(String(30))
    passwd = Column(String(400))
    nombre = Column(String(75))
    apell_p = Column(String(75))
    apell_m = Column(String(75))
    fecha_ingreso = Column(DateTime)

    def __init__(self):
        pass

    def __repr__(self):
        return "<User(%d, %d, '%s')>" % (self.id, self.rut, self.usuario)

class Venta (Base):
    __tablename__ = 'venta'

    id = Column(Integer, primary_key=True)
    monto = Column(Integer)
    fecha = Column(DateTime)
    maquina = Column(Integer)
    vendedor = Column(Integer)
    tipo_documento = Column(Integer)
    tipo_venta = Column(Integer)
    descuento = Column(Integer)
    id_documento = Column(Integer)
    canceled = Column(Boolean)

    def __init__ (self):
        pass

    def __repr__ (self):
        return "<Venta(%d)" % (self.id)

class VentaDetalle(Base):
    __tablename__ = "venta_detalle"

    id = Column(Integer, primary_key=True)
    id_venta = Column(Integer, ForeignKey('venta.id'))
    barcode = Column(Integer, ForeignKey('producto.barcode'))
    cantidad = Column(Float)
    precio = Column(Integer)
    fifo = Column(Integer)
    iva = Column(Integer)
    otros = Column(Integer)

    def __init__ (self):
        pass

    def __repr__ (self):
        return "VentaDetalle<%d>" % (self.id)

class Producto (Base):
    __tablename__ = "producto"

    barcode = Column(Integer, primary_key=True)
    codigo_corto = Column(String(10), unique=True)
    marca = Column(String(35))
    descripcion = Column(String(50))
    contenido = Column(String(10))
    unidad = Column(String(10))
    stock = Column(Float, default=0.0)
    precio = Column(Integer)
    costo_promedio = Column(Integer)
    vendidos = Column(Float)
    impuestos = Column(Boolean)
    otros = Column(Integer)
    familia = Column(Integer)
    perecibles = Column(Boolean)
    stock_min = Column(Float, default=0.0)
    margen_promedio = Column(Float, default=0.0)
    fraccion = Column(Boolean, default=False)
    canje = Column(Boolean, default=False)
    stock_pro = Column(Float, default=0.0)
    tasa_canje = Column(Float, default=1.0)
    precio_mayor = Column(Integer)
    cantidad_mayor = Column(Integer)
    mayorista = Column(Boolean, default=False)

    def __init__ (self):
        pass

    def __repr__ (self):
        return "Product<%d, '%s'>" % (self.barcode, self.codigo_corto)


class SalesRankingColumns(enum):
    OBJECT, DESCRIPTION, BRAND, CONTENT, UNIT, STOCK, UNITS_SOLD, COST, CONTRIB, MARGIN = range (10)


class SalesRankingListStore(gtk.ListStore):
    """This class extend the gtk.ListStore to be connected to the gtk.TreeView of the sales ranking.
    """

    def __init__(self, products):
        """

        Arguments:
        - `products`: list of Product class instances
        """
        gtk.ListStore.__init__(self, gobject.TYPE_OBJECT)
        self._products = products

        self._col_types = [gobject.TYPE_OBJECT,
                           gobject.TYPE_STRING,  # description
                           gobject.TYPE_STRING,  # brand
                           gobject.TYPE_STRING,  # content
                           gobject.TYPE_STRING,  # unit
                           gobject.TYPE_FLOAT,   # stock
                           gobject.TYPE_FLOAT,   # units sold
                           gobject.TYPE_FLOAT,   # cost
                           gobject.TYPE_FLOAT,   # contrib
                           gobject.TYPE_FLOAT]   # margin

    def get_n_columns(self):
        """Returns the number of columns supported by the model

        Arguments:
        - `self`:
        """
        assert self is SalesRankingListStore, \
               TypeError("must pass a SalesRankingListStore instance")

        return len(self._col_types)

    def get_column_type(self, index):
        """Returns the type of the column index

        Arguments:
        - `self`:
        - `index`:
        """
        assert (index is int) and (index>=0) and (index < self._N_COLUMNS), \
               RuntimeError("index must be an int and between 0 and %s" % (self._N_COLUMNS))

        return self._col_types[index]

    def get_value(self, iter, column):
        """Returns the value at column at the path pointed to by iter

        Arguments:
        - `self`:
        - `iter`:
        - `column`:
        """
        obj = gtk.ListStore.get_value (self, iter, 0)

        if column == SalesRankingColumns.OBJECT:
            return obj
        elif column == SalesRankingColumns.DESCRIPTION:
            return obj.descripcion
        elif column == SalesRankingColumns.BRAND:
            return obj.marca
        elif column == SalesRankingColumns.CONTENT:
            return obj.contenido
        elif column == SalesRankingColumns.UNIT:
            return obj.unidad
        elif column == SalesRankingColumns.STOCK:
            return obj.stock
        elif column == SalesRankingColumns.UNITS_SOLD:
            return obj.vendidos
        elif column == SalesRankingColumns.COST:
            return obj.get_cost()
        elif column == SalesRankingColumns.CONTRIB:
            return obj.get_contribution()
        elif column == SalesRankingColumns.MARGIN:
            return obj.get_margin()
        else:
            return None

gobject.type_register (SalesRankingListStore)


