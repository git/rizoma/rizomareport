##
## reports.py
## Login : <freyes@yoda>
## Started on  Tue Jun  3 15:11:20 2008 Felipe Reyes
## $Id$
##
## Copyright (C) 2008 Rizoma Tecnologia Limitada <info@rizoma.cl>
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
##

import gtk
import gobject
from datetime import *
from pkg_resources import resource_filename
from rizoma.rank_sell import *
from rizoma.rizoma_db import *

class calendario:
    def __init__ (self, entry):
        self.entry_date = entry
        previous_date = self.entry_date.get_text()

        window = gtk.Window (gtk.WINDOW_TOPLEVEL);

        window.connect ('focus-out-event',
                        self.on_calendar_focus_out,
                        None)

        calendar = gtk.Calendar ()
        window.add (calendar)

        calendar.connect ('day-selected-double-click',
                          self.on_calendar_day_selected_double_click,
                          None)

        window_gdk = self.entry_date.get_parent_window()
        (x, y) = window_gdk.get_origin()

        x = x + self.entry_date.allocation.x
        y = y + self.entry_date.allocation.y + self.entry_date.allocation.height

        if x < 0:
            x = 0
        if y < 0:
            y = 0

        window.move (x, y)
        window.show_all ()

    def on_calendar_day_selected_double_click (self, calendar, data=None):
        (year, month, day) = calendar.get_date ()

        fecha = date (year, month + 1, day)
        self.entry_date.set_text (fecha.isoformat())

        window = calendar.get_toplevel()
        window.destroy()

    def on_calendar_focus_out (self, window, event, data):
        window.destroy()


class MainWindow:
    def __init__(self):
        self.ui_file = resource_filename ('rizoma.ui', 'rizoma-report.ui')

        self.builder = gtk.Builder ()
        self.builder.add_from_file (self.ui_file)

        self.ntbk_reports = self.builder.get_object ('ntbk_reports')

        self.date_begin_entry = self.builder.get_object ('entry_date_begin')
        self.date_end_entry = self.builder.get_object ('entry_date_end')

        signals_dic = { "on_btn_quit_clicked" : gtk.main_quit,
                        "on_btn_get_stat_clicked" : self.on_btn_get_stat_clicked,
                        "display_calendar" : calendario}
        self.builder.connect_signals (signals_dic)

        store = gtk.ListStore (gobject.TYPE_STRING,
                               gobject.TYPE_STRING,
                               gobject.TYPE_STRING,
                               gobject.TYPE_STRING,
                               gobject.TYPE_STRING,
                               gobject.TYPE_STRING)
        tree = self.builder.get_object ("tree_view_sells")
        tree.set_model (store)

        tree_sel = tree.get_selection ()
        tree_sel.connect ('changed',
                          self.on_tree_selection_sell_changed,
                          None)

        column = gtk.TreeViewColumn ('Fecha')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 0)

        column = gtk.TreeViewColumn ('ID Venta')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 1)

        column = gtk.TreeViewColumn ('Maq.')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 2)

        column = gtk.TreeViewColumn ('Vendedor')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 3)

        column = gtk.TreeViewColumn ('Monto')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 4)

        column = gtk.TreeViewColumn ('Tipo Pago')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 5)

        store = gtk.ListStore (gobject.TYPE_STRING,
                               gobject.TYPE_STRING,
                               gobject.TYPE_STRING,
                               gobject.TYPE_STRING)
        tree = self.builder.get_object ("tree_view_sell_detail")
        tree.set_model (store);

        column = gtk.TreeViewColumn ('Producto')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 0)

        column = gtk.TreeViewColumn ('Cantidad')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 1)

        column = gtk.TreeViewColumn ('Unitario')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 2)

        column = gtk.TreeViewColumn ('Total')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 3)

        store = gtk.ListStore (gobject.TYPE_STRING,
                               gobject.TYPE_STRING,
                               gobject.TYPE_STRING,
                               gobject.TYPE_STRING,
                               gobject.TYPE_DOUBLE,
                               gobject.TYPE_INT,
                               gobject.TYPE_INT,
                               gobject.TYPE_INT,
                               gobject.TYPE_DOUBLE);
        tree = self.builder.get_object ("tree_view_sell_rank")
        tree.set_model (store);

        column = gtk.TreeViewColumn ('Producto')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 0)

        column = gtk.TreeViewColumn ('Marca')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 1)

        column = gtk.TreeViewColumn ('Medida')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 2)

        column = gtk.TreeViewColumn ('Unid.')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 3)

        column = gtk.TreeViewColumn ('Unidades')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 4)

        column = gtk.TreeViewColumn ('Vendido $')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 5)

        column = gtk.TreeViewColumn ('Costo $')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 6)

        column = gtk.TreeViewColumn ('Contrib $')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 7)

        column = gtk.TreeViewColumn ('Margen %')
        tree.append_column (column)
        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, True)
        column.add_attribute (renderer, 'text', 8)

        self.main_window = self.builder.get_object ('wnd_reports')
        self.main_window.show_all ()

        gtk.main()

    def on_tree_selection_sell_changed (self, selection, data):
        (model, iter) = selection.get_selected ()

        (sell_id,) = model.get (iter, 1)

        data = RizomaData ()
        rows = data.Execute ("SELECT descripcion ||' '|| marca ||' '|| contenido ||' '|| unidad as long_desc, cantidad, venta_detalle.precio, (cantidad * venta_detalle.precio)::int AS monto FROM venta_detalle, producto WHERE producto.barcode=venta_detalle.barcode and id_venta=%s" % sell_id)

        tree = self.builder.get_object ("tree_view_sell_detail")
        store = tree.get_model ()
        store.clear()

        for row in rows:
            store.append ( [
                    row['long_desc'],
                    row['cantidad'],
                    row['precio'],
                    row['monto']
                    ] )


    def on_btn_get_stat_clicked (self, button):
        current_page = self.ntbk_reports.get_current_page ()
        str_begin = self.date_begin_entry.get_text()
        str_end = self.date_end_entry.get_text()

        if current_page != 2 and len (str_begin) == 0 or len (str_end) == 0:
            return

        if current_page == 0:
            data = RizomaData ()
            rows = data.Execute ("SELECT id, maquina, vendedor, monto, to_char (fecha, 'DD/MM/YY HH:MM') as fmt_fecha, tipo_venta FROM venta WHERE fecha>= to_date ('%s', 'YYYY-MM-DD') and fecha <= to_date ('%s', 'YYYY-MM-DD') and id not in  (select id_sale from venta_anulada) ORDER BY fecha DESC" % (str_begin, str_end))

            tree = self.builder.get_object ('tree_view_sells')
            store = tree.get_model ()
            store.clear ()

            for row in rows:
                store.append ( [
                        row['fmt_fecha'],
                        row['id'],
                        row['maquina'],
                        row['vendedor'],
                        row['monto'],
                        row['tipo_venta']
                        ] )

            rows = data.Execute ("SELECT SUM ((SELECT SUM (cantidad * precio) FROM venta_detalle WHERE id_venta=venta.id)) as total, count (*) as total_n FROM venta WHERE fecha>=to_timestamp ('%s', 'YYYY-MM-DD') AND fecha <to_timestamp ('%s', 'YYYY-MM-DD') and (SELECT forma_pago FROM documentos_emitidos WHERE id=id_documento)=0" % (str_begin, str_end))
            for row in rows:
                total_cash = row['total']
                total_cash_n = row['total_n']

            rows = data.Execute ("SELECT SUM ((SELECT SUM (cantidad * precio) FROM venta_detalle WHERE id_venta=venta.id)) as total, count (*) as total_n FROM venta WHERE fecha>=to_timestamp ('%s', 'YYYY-MM-DD') AND fecha <to_timestamp ('%s', 'YYYY-MM-DD') and (SELECT forma_pago FROM documentos_emitidos WHERE id=id_documento) in (2,3)" % (str_begin, str_end))
            for row in rows:
                total_credit = row['total']
                total_credit_n = row['total_n']

            rows = data.Execute ("SELECT SUM ((SELECT SUM (cantidad * precio) FROM venta_detalle WHERE id_venta=venta.id)) as total, count (*) as total_n FROM venta WHERE fecha>=to_timestamp ('%s', 'YYYY-MM-DD') AND fecha <to_timestamp ('%s', 'YYYY-MM-DD') and (SELECT forma_pago FROM documentos_emitidos WHERE id=id_documento)=0" % (str_begin, str_end))
            for row in rows:
                total = row['total']
                total_n = row['total_n']

            if total_cash != None:
                self.builder.get_object ('lbl_sell_cash_amount').set_markup ('<span size="x-large">$ %d</span>' % total_cash)
                self.builder.get_object ('lbl_sell_cash_n').set_markup ('<span size="x-large">%d</span>' % total_cash_n)
                self.builder.get_object ('lbl_sell_cash_average').set_markup ('<span size="x-large">%d</span>' % (total_cash / total_cash_n))

            if total_credit != None:
                self.builder.get_object ('lbl_sell_credit_amount').set_markup ('<span size="x-large">$ %d</span>' % total_credit)
                self.builder.get_object ('lbl_sell_credit_n').set_markup ('<span size="x-large"> $ %d</span>' % total_credit_n)
                self.builder.get_object ('lbl_sell_credit_average').set_markup ('<span size="x-large"> $ %d</span>' % (total_credit / total_credit_n))

            if total != None:
                self.builder.get_object ('lbl_sell_total_amount').set_markup ('<span size="x-large"> $ %d</span>' % total)
                self.builder.get_object ('lbl_sell_total_n').set_markup ('<span size="x-large"> $ %d</span>' % total_n)
                self.builder.get_object ('lbl_sell_average').set_markup ('<span size="x-large"> $ %d</span>' % (total / total_n))

        elif current_page == 1:
            tree = self.builder.get_object ('tree_view_sell_rank')
            rank = RankSell (tree.get_model (), str_begin, str_end)

            (vendidos, costo, contrib) = rank.get_totals ()

            self.builder.get_object ('lbl_rank_sold').set_markup ('<span size="x-large">$ %d</span>' % vendidos)
            self.builder.get_object ('lbl_rank_cost').set_markup ('<span size="x-large">$ %d</span>' % costo)
            self.builder.get_object ('lbl_rank_contrib').set_markup ('<span size="x-large">$ %d</span>' % contrib)

            margen = (contrib / costo ) * 100

            if margen != 0:
                self.builder.get_object ('lbl_rank_margin').set_markup ('<span size="x-large">%d %%</span>' % margen)
            else:
                self.builder.get_object ('lbl_rank_margin').set_markup ('<span size="x-large">0 %%</span>')

        elif current_page == 2:
            cash_date = self.builder.get_object ('entry_money_box_date').get_text ()

            if len (cash_date) == 0:
                return

            data = RizomaData ()
            rows = data.Execute ("select to_char (open_date, 'DD-MM-YYYY HH24:MI') as open_date_formatted, to_char (close_date, 'DD-MM-YYYY HH24:MI') as close_date_formatted, * from cash_box_report (to_date ('%s', 'YYYY-MM-DD'))" % cash_date)

            for row in rows:
                self.builder.get_object ('lbl_open').set_text (row['open_date_formatted'])
                self.builder.get_object ('lbl_close').set_text (row['close_date_formatted'])
                self.builder.get_object ('lbl_mbox_start').set_text (str (row['cash_box_start']))
                self.builder.get_object ('lbl_mbox_cash').set_text (str (row['cash_sells']))
                self.builder.get_object ('lbl_mbox_payed_money').set_text (str (row['cash_payed_money']))
                self.builder.get_object ('lbl_mbox_other').set_text (str (row['cash_income']))
                self.builder.get_object ('lbl_mbox_ingr_total').set_text (str (row['cash_sells'] + row['cash_income']))
                self.builder.get_object ('lbl_mbox_out').set_text (str (row['cash_outcome']))
                self.builder.get_object ('lbl_mbox_out_total').set_text (str (row['cash_outcome']))

                self.builder.get_object ('lbl_money_box_total').set_text (str (
                    (row['cash_box_start'] + row['cash_sells'] + row['cash_income'] + row['cash_payed_money'])
                    -
                    (row['cash_outcome'])
                    ))
