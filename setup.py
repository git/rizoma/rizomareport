##
## setup.py
## Login : <freyes@yoda>
## Started on  Tue Jun  3 15:23:08 2008 Felipe Reyes
## $Id$
##
## Copyright (C) 2008 Rizoma Tecnologia Limitada <info@rizoma.cl>
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
##

from distutils.core import setup
import sys

required_modules = ('gtk',
                    'gobject',
                    'pkg_resources',
                    'psycopg2',
                    'pychart')

def check_modules ():
    ok = True
    for module in required_modules:
        try:
            exec ('import %s' % module)
        except ImportError:
            ok = False
            print 'Error: %s is required to build this package' % module

    if not ok:
        sys.exit(1)

check_modules ()

setup(name="Rizoma Reportes",
      version='0.1',
      description="A simple tool to display reports of POS rizoma comercio",
      author="Rizoma Tecnologia Ltda.",
      author_email="info@rizoma.cl",
      maintainer="Jonathan Gonzalez V.",
      maintainer_email="jonathan@rizoma.cl",
      url="http://www.rizoma.cl",
      packages=['rizoma'],
      package_data={
        'rizoma':
            ['ui/*']
        },
      scripts=['scripts/rizoma-reports.py']
      )
